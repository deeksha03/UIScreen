//
//  ViewController.swift
//  bill
//
//  Created by Apple on 12/05/1940 Saka.
//  Copyright © 1940 VVDN. All rights reserved.
//

import UIKit


class ViewController: UIViewController,UITextFieldDelegate {
   
    @IBOutlet weak var upBtn: NSLayoutConstraint!
    @IBOutlet weak var text1: UITextField!
    
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn1: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btn1.layer.cornerRadius = 8
        self.btn1.layer.borderWidth = 1
       self.btn1.layer.borderColor = UIColor.black.cgColor
        
        self.btn2.layer.cornerRadius = 8
        self.btn2.layer.borderWidth = 1
         self.btn2.layer.borderColor = UIColor.black.cgColor
        
        self.btn3.layer.cornerRadius = 8
        self.btn3.layer.borderWidth = 1
        
        self.btn1.titleLabel?.minimumScaleFactor = 0.1
        self.btn1.titleLabel?.numberOfLines = 1
        self.btn1.titleLabel?.adjustsFontSizeToFitWidth = true
        
        self.btn2.titleLabel?.minimumScaleFactor = 0.1
        self.btn2.titleLabel?.numberOfLines = 1
        self.btn2.titleLabel?.adjustsFontSizeToFitWidth = true
        
        self.btn3.titleLabel?.minimumScaleFactor = 0.1
        self.btn3.titleLabel?.numberOfLines = 1
        self.btn3.titleLabel?.adjustsFontSizeToFitWidth = true
        
        self.text1.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        text1.resignFirstResponder()
        return true
    }
   
    @objc func keyboardWillShow(notification: Notification) {
        DispatchQueue.main.async {

        if let userInfo = notification.userInfo as? Dictionary<String, AnyObject> {
          let frame = userInfo[UIKeyboardFrameEndUserInfoKey]
            let keyboardRect = frame?.cgRectValue
            if let keyboardHeight = keyboardRect?.height
            {
                    self.upBtn.constant = keyboardHeight
                    UIView.animate(withDuration: 0.0, animations: {
                        self.view.layer.removeAllAnimations()
                        
                    })
                
              
            }
        }
        }
    }
    @objc func keyboardWillHide(notification: Notification) {
         DispatchQueue.main.async {
        self.upBtn.constant = 38.0
        }
    }
}
