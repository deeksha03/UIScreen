//
//  fourViewController.swift
//  hello
//
//  Created by Apple on 18/05/1940 Saka.
//  Copyright © 1940 VVDN. All rights reserved.
//

import UIKit

class FourViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var text1: UITextField!
     @IBOutlet weak var text2: UITextField!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.text1.delegate = self
         self.text2.delegate = self
        
        text1.tag = 0
         text2.tag = 1
        
        btn1.layer.cornerRadius = 8
        btn1.layer.borderWidth = 1
        btn1.layer.borderColor = UIColor.black.cgColor
        
        btn2.layer.cornerRadius = 8
        btn2.layer.borderWidth = 1
        
        self.btn1.titleLabel?.minimumScaleFactor = 0.1
        self.btn1.titleLabel?.numberOfLines = 1
        self.btn1.titleLabel?.adjustsFontSizeToFitWidth = true
        
        self.btn2.titleLabel?.minimumScaleFactor = 0.1
        self.btn2.titleLabel?.numberOfLines = 1
        self.btn2.titleLabel?.adjustsFontSizeToFitWidth = true
        
      view1.backgroundColor = UIColor.black
        view2.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    }
    override func viewWillAppear(_ animated: Bool) {
        view1.backgroundColor = UIColor.black
   
    }
    @IBAction func textBegin1(_ sender: UITextField) {
         view1?.backgroundColor = UIColor.black
        
    }
    @IBAction func textBegin2(_ sender: UITextField) {
      view2.backgroundColor = UIColor.black
        
    }
  
    @IBAction func textField1(_ sender: UITextField) {
       view1?.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
    }
   
    @IBAction func textField2(_ sender: UITextField) {
        view2?.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
       
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
    
        } else {
            textField.resignFirstResponder()
        }
        return true
    }

    
}
