//
//  ThirdViewController.swift
//  hello
//
//  Created by Apple on 18/05/1940 Saka.
//  Copyright © 1940 VVDN. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btn1.layer.cornerRadius = 8
        btn1.layer.borderWidth = 1
        btn1.layer.borderColor = UIColor.black.cgColor
        
        btn2.layer.cornerRadius = 8
        btn2.layer.borderWidth = 1
        
        self.btn1.titleLabel?.minimumScaleFactor = 0.1
        self.btn1.titleLabel?.numberOfLines = 1
        self.btn1.titleLabel?.adjustsFontSizeToFitWidth = true
        
        self.btn2.titleLabel?.minimumScaleFactor = 0.1
        self.btn2.titleLabel?.numberOfLines = 1
        self.btn2.titleLabel?.adjustsFontSizeToFitWidth = true
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
